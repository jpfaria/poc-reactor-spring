package br.com.jpfaria.poc.consumer;

import br.com.jpfaria.poc.util.Consts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;

@Consumer
@Slf4j
public class Test2Consumer {

    @Autowired
    private EventBus eventBus;

    @Selector(value = Consts.TOPIC1)
    public void onTopic(Event<String> s) {
        log.info("onTopic: [" + s.getData() + "]");
    }

}
