package br.com.jpfaria.poc.controller;

import br.com.jpfaria.poc.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/test1", produces = "application/json")
public class Test1Controller {

    @Autowired
    private TestService test1Service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String test(@RequestParam String message) throws InterruptedException {
        test1Service.test();
        return "OK";
    }


}

