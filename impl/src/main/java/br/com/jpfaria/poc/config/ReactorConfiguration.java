package br.com.jpfaria.poc.config;


import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import reactor.Environment;
import reactor.bus.EventBus;
import reactor.jarjar.com.lmax.disruptor.YieldingWaitStrategy;
import reactor.jarjar.com.lmax.disruptor.dsl.ProducerType;
import reactor.spring.context.config.EnableReactor;
import reactor.spring.core.task.WorkQueueAsyncTaskExecutor;

import java.util.concurrent.CountDownLatch;

@Configuration
@EnableReactor
@Log4j
@ComponentScan("reactor.spring")
public class ReactorConfiguration {

    @Bean
    public Environment env() {
        return new Environment();
    }

    @Bean
    public EventBus reactor(Environment env) {
        log.info("Create Reactor EventBus Bean...");
        return EventBus.config().env(env).dispatcher(Environment.THREAD_POOL).get();
        //return EventBus.config().env(env).get();
    }

    public AsyncTaskExecutor workQueueAsyncTaskExecutor(Environment env) {

        WorkQueueAsyncTaskExecutor taskExecutor = new WorkQueueAsyncTaskExecutor(env);
        taskExecutor.setName("workQueueExecutor");
        taskExecutor.setBacklog(2048);
        taskExecutor.setThreads(4);
        taskExecutor.setProducerType(ProducerType.SINGLE);
        taskExecutor.setWaitStrategy(new YieldingWaitStrategy());

        return taskExecutor;
    }


}
