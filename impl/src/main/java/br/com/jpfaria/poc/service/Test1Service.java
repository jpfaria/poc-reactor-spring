package br.com.jpfaria.poc.service;

import br.com.jpfaria.poc.util.Consts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.Environment;
import reactor.bus.Event;
import reactor.bus.EventBus;

@Service
@Slf4j
public class Test1Service implements TestService {

    @Autowired
    private EventBus reactor;

    private int numberOfQuotes = 100;

    private long start;

    @Autowired
    Environment env;

    public void test() throws InterruptedException {

        start = System.currentTimeMillis();

        for (int i = 0; i < numberOfQuotes; i++) {
            reactor.notify(Consts.TOPIC1, Event.wrap("Message:" + i));
        }

        /*
        Promise<String> p = Promises.success(Consts.TOPIC1);
        p.onComplete(new Consumer<Promise<String>>() {

            @Override
            public void accept(Promise<String> stringPromise) {

            }
        });
        */

        long elapsed = System.currentTimeMillis() - start;

        log.info("Elapsed time: " + elapsed + "ms");
        log.info("Average time per quote: " + elapsed / numberOfQuotes + "ms");


    }

}
