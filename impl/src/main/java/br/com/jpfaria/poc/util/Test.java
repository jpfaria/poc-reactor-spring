package br.com.jpfaria.poc.util;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = {"name"})
@Builder
public class Test {
    private String name;
    private String endereco;
}
